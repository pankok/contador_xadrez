import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:contador_xadrez/bloc/bloc.dart';
import 'package:contador_xadrez/dadotempo.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      darkTheme: ThemeData(
        primaryColor: Color.fromRGBO(109, 23, 255, 1),
        accentColor: Color.fromRGBO(72, 74, 126, 1),
        brightness: Brightness.dark,
      ),
      theme: ThemeData(
        primaryColor: Color.fromRGBO(109, 23, 255, 1),
        accentColor: Color.fromRGBO(22, 43, 229, 1),
        brightness: Brightness.light,
      ),
      home: BlocProvider(
        builder: (context) => TimerBloc(ticker: Ticker()),
        child: Timer(),
      ),
      
    );
  }
}

class Timer extends StatelessWidget {
  static const TextStyle timerTextStyle = TextStyle(
    fontSize: 60,
    fontWeight: FontWeight.bold,
  );
 
  @override
  Widget build(BuildContext context) {
    _portraitModeOnly();
        return Scaffold(
          body: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:100),
                    child:Center(
                      child:BlocBuilder<TimerBloc, TimerState>(
                        condition: (previousState, state) =>
                            state.runtimeType != previousState.runtimeType,
                        builder: (context, state) => Actions(),
                      ),
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 70.0),
                    child: Center(
                      child: BlocBuilder<TimerBloc, TimerState>(
                        builder: (context, state) {
                          final String secondsStr = (state.duration % 60)
                              .floor()
                              .toString()
                              .padLeft(2, '0');
                          return Text(
                            '$secondsStr',
                            style: Timer.timerTextStyle,
                          );
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:70),
                    child:Center(
                      child:BlocBuilder<TimerBloc, TimerState>(
                        condition: (previousState, state) =>
                            state.runtimeType != previousState.runtimeType,
                        builder: (context, state) => Actions(),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      }
    }
    
  void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
  ]);
  }

class Actions extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: _mapStateToActionButtons(
        timerBloc: BlocProvider.of<TimerBloc>(context),
      ),
    );
  }
  
  List<Widget> _mapStateToActionButtons({
    TimerBloc timerBloc,
  }) {
    final TimerState currentState = timerBloc.state;
    if (currentState is Ready) {
      return [
        FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: () =>
              timerBloc.add(Start(duration: currentState.duration)),
        ),
      ];
    }
    if (currentState is Running) {
      return [
        FloatingActionButton(
          child: Icon(Icons.replay),
          backgroundColor: Colors.red,
          onPressed: () => timerBloc.add(Reset()),
        ),
      ];
    }
    if (currentState is Finished) {
      return [
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => timerBloc.add(Reset()),
        ),
      ];
    }
    return [];
  }
}